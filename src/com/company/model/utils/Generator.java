package com.company.model.utils;

import com.company.model.organizations.*;

import java.util.ArrayList;

public class Generator {

    public ArrayList<CurrencyConverter> getCurrencyConverters() {
        ArrayList<CurrencyConverter> converters = new ArrayList<>();
        converters.add(new Exchanger("Копейка", "Pushkina"));
        converters.add(new Bank("Privat", "Kolotushkina", 12_000, 15f));

        return converters;
    }

    public ArrayList<Credit> giveCredit() {
        ArrayList<Credit> credits = new ArrayList<>();
        credits.add(new Lombard("Blago", "Pushkina", 50_000, 0.4f, 12));
        credits.add(new CreditCafe("KreditCafe", "Garibaldi", 4_000, 2, 12));
        credits.add(new AllianceCredit("AllianceCredit", "Artema", 100_000, 0.2f, 12));
        credits.add(new Bank("Privat", "Artema", 200_000, 0.25f, 12));

        return credits;
    }

    public ArrayList<Deposit> giveDeposit() {
        ArrayList<Deposit> deposits = new ArrayList<>();
        deposits.add(new PIF("PIF", "Shevhenko st.", 15, 0.18f, 1996));
        deposits.add(new Bank("Privat", "Artema st.", 0.15f, 6, 1990));

        return deposits;
    }

    public ArrayList<SendMoney> sendMonies() {
        ArrayList<SendMoney> sendMoneyArrayList = new ArrayList<>();
        sendMoneyArrayList.add(new Post("Post of Ukraine", "Sumskaya st.", 0.02f));
        sendMoneyArrayList.add(new Bank("Privat", "Artema st.", 0.02f));

        return sendMoneyArrayList;
    }
}
