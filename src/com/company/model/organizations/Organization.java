package com.company.model.organizations;

public class Organization {
    protected String name;
    protected String address;

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
