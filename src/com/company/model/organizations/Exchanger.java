package com.company.model.organizations;

import com.company.model.currency.ExchangeCurrency;

import java.util.ArrayList;

public class Exchanger extends Organization implements CurrencyConverter {

    public Exchanger(String name, String address) {
        super(name, address);
    }

    private ArrayList<ExchangeCurrency> getExchangeCurrencies() {
        ArrayList<ExchangeCurrency> currenciesExchanger = new ArrayList<>();
        currenciesExchanger.add(new ExchangeCurrency("UAH", 26.5f, "USD"));
        currenciesExchanger.add(new ExchangeCurrency("UAH", 29.5f, "EUR"));
        currenciesExchanger.add(new ExchangeCurrency("USD", 26.0f, "UAH"));
        currenciesExchanger.add(new ExchangeCurrency("EUR", 29.0f, "UAH"));
        return currenciesExchanger;
    }

    @Override
    public float convert(int sum, String fromCurrency, String toCurrency) {  //Ctrl + I for currency converter implementation

        ArrayList<ExchangeCurrency> currencies = getExchangeCurrencies();

        ExchangeCurrency pairCurrency = null;

        for (ExchangeCurrency currency : currencies) {
            if (currency.getName().equals(fromCurrency) && currency.getToCurrencyName().equals(toCurrency)) {
                pairCurrency = currency;
            }
        }
        return sum * pairCurrency.getRate();
    }
}
