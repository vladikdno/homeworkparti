package com.company.model.organizations;

public interface Credit {
    float giveCredit(Integer sum);
}
