package com.company.model.organizations;

public interface SendMoney {
    float sendMoney(Float sum);
}
