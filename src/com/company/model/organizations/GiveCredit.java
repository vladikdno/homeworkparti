package com.company.model.organizations;

public abstract class GiveCredit extends Organization implements Credit {
    private int limitInUAH;
    private float procent;
    private int months;

    public GiveCredit(String name, String address, int limitInUAH, float procent, int months) {
        super(name, address);
        this.limitInUAH = limitInUAH;
        this.procent = procent;
        this.months = months;
    }

    @Override
    public float giveCredit(Integer sum) {
        if (sum < limitInUAH) {
            return procent * months + sum;
        } else {
            return 0f;
        }
    }
}
