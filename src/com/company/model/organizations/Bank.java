package com.company.model.organizations;
import com.company.model.currency.ExchangeCurrency;
import java.util.ArrayList;

public class Bank extends Organization implements CurrencyConverter, Credit, Deposit, SendMoney {
    private float limit;
    private float procent;
    private int months;
    private int licenseYear;

    public Bank(String name, String address, float limit, float procent, int months) {// for credit
        super(name, address);
        this.limit = limit;
        this.procent = procent;
        this.months = months;
    }

    public Bank(String name, String address, float procent, int months, int licenseYear) {//for deposit
        super(name, address);
        this.procent = procent;
        this.months = months;
        this.licenseYear = licenseYear;
    }

    public Bank(String name, String address, float procent) {//for send money
        super(name, address);
        this.procent = procent;
    }

    public Bank(String name, String address, float limit, float procent) {
        super(name, address);
        this.limit = limit;
        this.procent = procent;
    }

    private ArrayList<ExchangeCurrency> getExchangeCurrencies() {
        ArrayList<ExchangeCurrency> currenciesBank = new ArrayList<>();
        currenciesBank.add(new ExchangeCurrency("UAH", 26.5f, "USD"));
        currenciesBank.add(new ExchangeCurrency("UAH", 0.41f, "RUB"));
        currenciesBank.add(new ExchangeCurrency("UAH", 29.5f, "EUR"));
        currenciesBank.add(new ExchangeCurrency("USD", 25.5f, "UAH"));
        currenciesBank.add(new ExchangeCurrency("RUB", 0.31f, "UAH"));
        currenciesBank.add(new ExchangeCurrency("EUR", 28.5f, "UAH"));
        return currenciesBank;
    }


    @Override
    public float convert(int sum, String fromCurrency, String toCurrency) {
        ArrayList<ExchangeCurrency> currenciesBank = getExchangeCurrencies();

        ExchangeCurrency pairCurrency = null;

        for (ExchangeCurrency currency : currenciesBank) {
            if (currency.getName().equals(fromCurrency) && currency.getToCurrencyName().equals(toCurrency)) {
                pairCurrency = currency;
            }
        }
        return sum * pairCurrency.getRate();
    }

    @Override
    public float giveCredit(Integer sum) {
        if (sum < limit) {
            return procent * months + sum;
        } else {
            return 0f;
        }

    }

    @Override
    public float acceptMoney(Integer sum) {
        if (months < 12) {
            return procent * months + sum;
        }
        return sum;
    }

    @Override
    public float sendMoney(Float sum) {
        return ((sum * procent) - 5) + sum;
    }
}
