package com.company.model.organizations;

public class CreditCafe extends GiveCredit {
    public CreditCafe(String name, String address, int limitInUAH, float procent, int months) {
        super(name, address, limitInUAH, procent, months);
    }

    @Override
    public float giveCredit(Integer sum) {
        return super.giveCredit(sum);
    }


    //not sure for use old version
    //public class CreditCafe extends Organization implements Credit {
//    int limitInUAH;
//    float procent;
//
//    public CreditCafe(String name, String address, int limitInUAH, float procent) {
//        super(name, address);
//        this.limitInUAH = limitInUAH;
//        this.procent = procent;
//    }
//
//    @Override
//    public float giveCredit(Integer sum) {
//        return sum * procent;
//    }
}
