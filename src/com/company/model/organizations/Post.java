package com.company.model.organizations;

public class Post extends Organization implements SendMoney {
    private float commission;

    public Post(String name, String address, float commission) {
        super(name, address);
        this.commission = commission;
    }

    @Override
    public float sendMoney(Float sum) {
        return sum * commission;
    }
}
