package com.company.model.organizations;

public interface Deposit {
    float acceptMoney(Integer sum);
}
