package com.company.model.organizations;

public class AllianceCredit extends GiveCredit {

    public AllianceCredit(String name, String address, int limitInUAH, float procent, int months) {
        super(name, address, limitInUAH, procent, months);
    }

    @Override
    public float giveCredit(Integer sum) {
        return super.giveCredit(sum);
    }
}
