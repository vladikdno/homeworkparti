package com.company.model.organizations;

public interface CurrencyConverter {
    float convert(int sum, String fromCurrency, String toCurrency);
}
