package com.company.model.organizations;

public abstract class GetDeposit extends Organization implements Deposit {
    protected int months;
    protected float procent;
    protected int yearFoundation;

    public GetDeposit(String name, String address, int months, float procent, int yearFoundation) {
        super(name, address);
        this.months = months;
        this.procent = procent;
        this.yearFoundation = yearFoundation;
    }

    @Override
    public float acceptMoney(Integer sum) {
        if (months > 12) {
            return procent * months + sum;
        }
        return sum;
    }
}
