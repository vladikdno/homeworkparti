package com.company.model.currency;

public class Currency {
    protected String name;

    public Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
