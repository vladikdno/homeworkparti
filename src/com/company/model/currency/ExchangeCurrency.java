package com.company.model.currency;

public class ExchangeCurrency extends Currency {
    protected float rate;
    protected String toCurrencyName;

    public ExchangeCurrency(String name, float rate, String toCurrencyName) {
        super(name);
        this.rate = rate;
        this.toCurrencyName = toCurrencyName;
    }

    public float getRate() {
        return rate;
    }

    public String getToCurrencyName() {
        return toCurrencyName;
    }
}
