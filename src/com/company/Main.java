package com.company;

import com.company.model.organizations.CurrencyConverter;
import com.company.model.organizations.Exchanger;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        System.out.println("Input your currency");
        Scanner scanner = new Scanner(System.in);
        String fromCurrency = scanner.nextLine();

        System.out.println("To currency.");
        String toCurrency = scanner.nextLine();

        System.out.println("How much?");

        int amount = Integer.parseInt(scanner.nextLine());

        CurrencyConverter exchanger = new Exchanger("Копейка", "Pushkina");

        float result = exchanger.convert(amount, fromCurrency, toCurrency);
        System.out.println(result);

    }
}
